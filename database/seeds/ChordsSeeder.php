<?php

use Illuminate\Database\Seeder;

class ChordsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('chords')->delete();
        DB::table('chords')->insert([

            0 =>
            array(
                'name' => 'C'
            ),

            1 =>
            array(
                'name' => 'Cm'
            ),

            2 =>
            array(
                'name' => 'C#'
            ),

            3 =>
            array(
                'name' => 'C#m'
            ),

            4 =>
            array(
                'name' => 'D'
            ),

            5 =>
            array(
                'name' => 'Dm'
            ),

            6 =>
            array(
                'name' => 'D#'
            ),

            7 =>
            array(
                'name' => 'D#m'
            ),

            8 =>
            array(
                'name' => 'E'
            ),

            9 =>
            array(
                'name' => 'Em'
            ),

            10 =>
            array(
                'name' => 'F'
            ),

            11 =>
            array(
                'name' => 'Fm'
            ),

            12 =>
            array(
                'name' => 'F#'
            ),

            13 =>
            array(
                'name' => 'F#m'
            ),

            14 =>
            array(
                'name' => 'G'
            ),

            15 =>
            array(
                'name' => 'Gm'
            ),

            16 =>
            array(
                'name' => 'G#'
            ),

            17 =>
            array(
                'name' => 'G#m'
            ),

            18 =>
            array(
                'name' => 'A'
            ),

            19 =>
            array(
                'name' => 'Am'
            ),

            20 =>
            array(
                'name' => 'A#'
            ),

            21 =>
            array(
                'name' => 'A#m'
            ),

            22 =>
            array(
                'name' => 'B'
            ),

            23 =>
            array(
                'name' => 'Bm'
            ),

            3 =>
            array(
                'name' => 'C#m'
            ),
        ]);
    }
}
