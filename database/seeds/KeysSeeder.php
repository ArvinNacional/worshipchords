<?php

use Illuminate\Database\Seeder;

class KeysSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('keys')->delete();
        DB::table('keys')->insert([

            0 =>
            array(
                'name' => 'Ab',
                'value' => '1'
            ),

            1 =>
            array(
                'name' => 'A',
                'value' => '2'
            ),

            2 =>
            array(
                'name' => 'A#',
                'value' => '3'
            ),

            3 =>
            array(
                'name' => 'Bb',
                'value' => '3'
            ),

            4 =>
            array(
                'name' => 'B',
                'value' => '4'
            ),

            5 =>
            array(
                'name' => 'C',
                'value' => '5'
            ),

            6 =>
            array(
                'name' => 'C#',
                'value' => '6'
            ),

            7 =>
            array(
                'name' => 'Db',
                'value' => '6'
            ),

            8 =>
            array(
                'name' => 'D',
                'value' => '7'
            ),

            9 =>
            array(
                'name' => 'D#',
                'value' => '8'
            ),

            10 =>
            array(
                'name' => 'Eb',
                'value' => '8'
            ),

            11 =>
            array(
                'name' => 'E',
                'value' => '9'
            ),

            12 =>
            array(
                'name' => 'F',
                'value' => '10'
            ),

            13 =>
            array(
                'name' => 'F#',
                'value' => '11'
            ),

            14 =>
            array(
                'name' => 'Gb',
                'value' => '11'
            ),

            
            15 =>
            array(
                'name' => 'G',
                'value' => '12'
            ),

            
            16 =>
            array(
                'name' => 'G#',
                'value' => '1'
            ),
        ]);
    }
}
