<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/all-songs', 'SongController@index');
Route::get('/add-song', 'SongController@create');
Route::post('/add-song', 'SongController@store');
Route::get('/song/{songId}','SongController@show');
Route::get('/edit-song/{songId}','SongController@edit');
Route::patch('/update-song/{songId}','SongController@update');
Route::delete('/delete-song', 'SongController@destroy');

Route::get('/home', 'HomeController@index')->name('home');


