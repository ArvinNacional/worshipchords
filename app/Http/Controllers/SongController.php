<?php

namespace App\Http\Controllers;

use App\Song;
use App\Key;
use Illuminate\Http\Request;

class SongController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $songs = Song::all();

        return view('all-songs', compact('songs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $keys = Key::all();

        return view('add-songs', compact('keys'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $song = new Song;
        $song->title = $request->title;
        $song->key_id = $request->key_id;
        $song->lyrics = $request->lyrics;
        $song->save();

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Song  $song
     * @return \Illuminate\Http\Response
     */
    public function show($songId, Request $request)
    {
        $song = Song::find($songId);
        $keys = Key::all();

        return view('song', compact('song','keys'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Song  $song
     * @return \Illuminate\Http\Response
     */
    public function edit($songId)
    {
        $song = Song::find($songId);
        $keys = Key::all();

        return view('edit-song', compact('song','keys'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Song  $song
     * @return \Illuminate\Http\Response
     */
    public function update($songId, Request $request)
    {
        $song = Song::find($songId);
        $song->title = $request->title;
        $song->key_id = $request->key_id;
        $song->lyrics = $request->lyrics;
        $song->save();

        return redirect('all-songs');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Song  $song
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id =  $request->song_id;
        $song = Song::find($id);
        $song->delete();

        return redirect('all-songs');
    }
}
