@extends('layouts.template')
@section('title','Songs')
@section('content')
    <h1 class="text-center py-3">All Songs</h1>
    <div class="container">
        <div class="row">
            <div class="col-lg-10">
                <div class="row">
                    <ul>
                    @foreach($songs as $song)
                        <li><a href="/song/{{$song->id}}">{{$song->title}}</a></li>              
                    @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection