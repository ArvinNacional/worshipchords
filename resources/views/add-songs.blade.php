@extends('layouts.template')
@section('title','Add-Songs')
@section('content')
    <h1 class="text-center">Add song</h1>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 offset-lg-4">
                <form action="/add-song" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class='form-group'>
                        <label for="title">Title</label>
                        <input type="text" name="title" class="form-control">
                    </div>
                    <div class='form-group'>
                        <label for="lyrics">Lyrics</label>
                        <textarea type="text" name="lyrics" class="form-control resize-ta"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="key_id">Key:</label>
                        <select name="key_id" class="form-control">
                            @foreach($keys as $key)
                                <option value="{{$key->id}}">{{$key->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="text-center">
                        <button class="btn btn-info">Add Song</button>
                    </div>

                    <br><br><br><br>
            </div>
        
        
        
        
        </div>
    
    
    </div>
    <script>function calcHeight(value) {
    let numberOfLineBreaks = (value.match(/\n/g) || []).length;
    // min-height + lines x line-height + padding + border
    let newHeight = 20 + numberOfLineBreaks * 20 + 12 + 2;
    return newHeight;
}

let textarea = document.querySelector(".resize-ta");
textarea.addEventListener("keyup", () => {
    textarea.style.height = calcHeight(textarea.value) + "px";
});</script>

@endsection