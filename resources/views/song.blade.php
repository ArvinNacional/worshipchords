@extends('layouts.template')
@section('title','Songs')
@section('content')

        <div class="container pt-5">
            <div class="row">
                <div class="col-lg-4 offset-lg-4">
                    <h3>Title: {{$song->title}}</h3>
                    
                    <h4>Key:  {{$song->key->name}}</h4>
                    <form action="">
                        <h4>Transpose </h4>
                        <div class="form-group">
                            <select name="" class="form-control">
                                @foreach($keys as $key)
                                    <option value="{{$key->id}}">{{$key->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </form>

                    
                    <p style="white-space:pre-wrap">{{$song->lyrics}}</p>
                    <div class="row">
                <div class="col-lg-6">
                    <a href="/edit-song/{{$song->id}}" class="btn btn-info" style="width: 150px;">Edit</a>
                </div>
                <div class="col-lg-6">
                    <form action="/delete-song" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="hidden" name="song_id" value="{{$song->id}}">
                        <button class="btn btn-danger">Delete</button>
                    </form>
                    
                </div>
            </div>
                </div>
                
            </div>
            
            <br>
            <br>
            <br>
            <br>
        </div>




@endsection