<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/bootswatch.css">

        <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- title -->
        <title>@yield('title')</title>

</head>
<body >

<!-- content -->
@yield("content")

<!-- footer -->
<footer class="footer bg-light fixed-bottom">
<p class="text-center">Made with Love By: Arvin</p>
</footer>

<script src="/js/app.js"></script>
</body>
</html>